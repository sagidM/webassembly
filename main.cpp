#include <emscripten/emscripten.h>
#include <iostream>
#include <string>

void callback(char* data, int size, void* arg) {
    std::cout << data << " | " << size << " (callback with arg: " << reinterpret_cast<int>(arg) << ")\n";
}

int main()
{
    worker_handle worker = emscripten_create_worker("cpp_worker.js");

    const char* str = "hello from main";
    emscripten_call_worker(worker, "func_in_worker", const_cast<char*>(str), strlen(str), callback, reinterpret_cast<void*>(123));

    std::cout << "main finishes\n";
    return 0;
}
