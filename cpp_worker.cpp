#include <iostream>
#include <string>
#include <emscripten/emscripten.h>

extern "C" {
    void func_in_worker(char* data, int size) {
        std::cout << "Code is running in cpp_worker with data: '" << data << "' | size: " << size << "\n";

        emscripten_worker_respond_provisionally("first respond", 13);
        emscripten_worker_respond_provisionally("second respond", 14);
        emscripten_worker_respond_provisionally("third respond", 13);
        emscripten_worker_respond("final respond", 13);
    }
}
