console.log('this code is running in my_worker.js')
onmessage = function(e) {
    const {width, height} = e.data
    const offscreen = new OffscreenCanvas(width, height)

    const ctx = offscreen.getContext("2d")
    ctx.fillStyle = 'red'
    ctx.fillRect(10, 10, 100, 100)
    
    const bitmap = offscreen.transferToImageBitmap()
    postMessage({bitmap}, [bitmap])
    console.log('in worker onmessage', ctx)
}

